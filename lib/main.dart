import 'package:flutter/material.dart';
import 'package:news_feed/deadline.dart';

void main() {
  runApp(MaterialApp(
    home: NewsFeed(),
  ));
}

class NewsFeed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: Center(
          child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            IconButton(
              onPressed: (){
                print("home icon");
              },
              icon: Icon(Icons.home),
            ),
            IconButton(
                onPressed: (){
                  print("friend request icon");
                },
                icon: Icon(Icons.person_add_alt)
            ),
            IconButton(
              icon:Icon(Icons.video_call),
              onPressed: (){
                print("video section icon");
              },
            ),
            IconButton(
              onPressed: (){
                print("user icon");
              },
              icon: Icon(Icons.verified_user)
            )
          ],
        ),
        ),
      ),
      body: ListView(
        children: [
          Card(
            child: Container(
              height: 300,
              child: Column(
                children: [
                  ListTile(
                    leading: CircleAvatar(
                      backgroundImage: AssetImage("images/user1.png"),
                    ),
                    onTap: (){
                      print("the listtile is clicked");
                    },
                    title: Text("user 1"),
                    subtitle: Text("march 31 2021"),
                    trailing: Icon(Icons.more_horiz),
                  ),
                  Text("Deadlines helps you to keep moving and deliver"
                      "content faster and it will increase your efficency as well."),
                  SizedBox(
                    height: 16,
                  ),
                  Expanded(
                    child: Image.asset("images/b.png")
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Row(
                          children: [
                            IconButton(
                                icon: Icon(Icons.thumb_up_alt),
                                onPressed: (){},
                            ),
                            Text("Like")
                          ],
                        ),
                        Row(
                          children: [
                            IconButton(
                              icon: Icon(Icons.read_more),
                              onPressed: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context) => Deadline()));
                              }),
                            Text("Read more")
                          ],
                        ),
                        Row(
                          children: [
                            IconButton(
                              icon: Icon(Icons.share),
                              onPressed: (){}),
                            Text("Share")
                          ],
                        )
                      ],
                  )
                ],
              ),
            ),
          ),
          Card(
            child: Container(
              height: 300,
              child: Column(
                children: [
                  ListTile(
                    leading: CircleAvatar(
                      backgroundImage: AssetImage("images/user1.png"),
                    ),
                    onTap: (){
                      print("the listtile is clicked");
                    },
                    title: Text("user 1"),
                    subtitle: Text("march 31 2021"),
                    trailing: Icon(Icons.more_horiz),
                  ),
                  Text("Deadlines helps you to keep moving and deliver"
                      "content faster and it will increase your efficency as well."),
                  SizedBox(
                    height: 16,
                  ),
                  Expanded(
                      child: Image.asset("images/b.png")
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Row(
                        children: [
                          IconButton(
                            icon: Icon(Icons.thumb_up_alt),
                            onPressed: (){},
                          ),
                          Text("Like")
                        ],
                      ),
                      Row(
                        children: [
                          IconButton(
                              icon: Icon(Icons.read_more),
                              onPressed: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context) => Deadline()));
                              }),
                          Text("Read more")
                        ],
                      ),
                      Row(
                        children: [
                          IconButton(
                              icon: Icon(Icons.share),
                              onPressed: (){}),
                          Text("Share")
                        ],
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
          Card(
            child: Container(
              height: 300,
              child: Column(
                children: [
                  ListTile(
                    leading: CircleAvatar(
                      backgroundImage: AssetImage("images/user1.png"),
                    ),
                    onTap: (){
                      print("the listtile is clicked");
                    },
                    title: Text("user 1"),
                    subtitle: Text("march 31 2021"),
                    trailing: Icon(Icons.more_horiz),
                  ),
                  Text("Deadlines helps you to keep moving and deliver"
                      "content faster and it will increase your efficency as well."),
                  SizedBox(
                    height: 16,
                  ),
                  Expanded(
                      child: Image.asset("images/b.png")
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Row(
                        children: [
                          IconButton(
                            icon: Icon(Icons.thumb_up_alt),
                            onPressed: (){},
                          ),
                          Text("Like")
                        ],
                      ),
                      Row(
                        children: [
                          IconButton(
                              icon: Icon(Icons.read_more),
                              onPressed: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context) => Deadline()));
                              }),
                          Text("Read more")
                        ],
                      ),
                      Row(
                        children: [
                          IconButton(
                              icon: Icon(Icons.share),
                              onPressed: (){}),
                          Text("Share")
                        ],
                      )
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          print("hey I'm Clicked");
        },
        child: Icon(Icons.access_alarms_outlined),
        backgroundColor: Colors.amber,
      ),
    );
  }
}


